
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//validate email
var validateEmail = function(email) {
    var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email)
};

var userSchema = new Schema({

  full_name: { type: String,  required: [true, 'Full name must be provided'] },
  email: { type: String,
    Required:  'Email address cannot be left blank.',
    validate: [validateEmail, 'Please fill a valid email address'],
         match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address'],
    index: {unique: true, dropDups: true}
    },

  password: { type: String , required: [true,  'Password cannot be left blank']},
  dob: { type: Date , required: [true, 'Date of birth must be provided']},

}, { collection : 'user' });

//authenticate input against database
userSchema.statics.authenticate = function(email, password, callback)
{
  User.findOne({email:email}).exec(function(err, user)
  {
    if(err)
    {
      return callback(err)
    } else if(!user)
    {
      var err = new Error('User not found.');
      err.status = 401;
      return callback(err);
    }
    if(password == user.password)
    {
      return callback(null, user);
    }
    else {
      return callback();
    }
  })
};


//var User = mongoose.model('Users', userSchema);
module.exports = mongoose.model('User', userSchema);
