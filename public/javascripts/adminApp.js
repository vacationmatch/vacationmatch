
var lodging = "";
var approve = "";
var reject ="" ;
var id = "";
var idDiv="";
$(function(){

    $("#loginAdmin").on('click', function(event){
        event.preventDefault();
        var email      = $("#emailLogin").val();
        var password   = $("#passwordLogin").val();

        if( !email || !password){

            $("#msgDiv").show().html("All fields are required.");

        }
        else{

            $.ajax({
                url: "user/adminLogin",
                method: "POST",
                data: { email: email, password: password }

            }).done(function(data) {

              var status = (data.status == 'error');
              var message = data.message;
              if(status){

                $("#msgDiv").show().html(message);
              }
              else{
                $("#msgDiv").removeClass('alert-danger').addClass('alert-success').show().html("You are logged in!");
                window.location = ('/lodging');
              }
            });
        }
    });
});
$(function(){
  $(document).on('click', 'button[data-id]', function (e) {
    event.preventDefault();
      var requested_to = $(this).attr('data-id')
      id = requested_to;
      lodging = "#"+requested_to+"3";
      approve = requested_to+"1";
      reject ="#" +requested_to+"2"
      idDiv = "#"+requested_to;

      console.log(lodging);
      console.log(approve);
      console.log(reject);
      console.log(idDiv);
      $(approve).show();
      $(reject).show();
      $(lodging).hide();

      $.ajax({
          url: "lodging/getLodging",
          method: "POST",
          data: {id: id}
      }).done(function(data) {

        if(data.status == 'error')
        {
          var errors = '<ul>';
          $.each( data.message, function( key, value )
          {
            errors = errors +'<li>'+value.msg+'</li>';
          });
          errors = errors+ '</ul>';
        }
        else
        {
          // window.location='/viewLodge';
          idDiv = "#"+id;
          $(idDiv).show();

        }
    });
});
});

function approveLodging()
{
      event.preventDefault();
      var url = "/lodging/update/"+id;
      console.log(url);
      $.ajax({
          url: url,
          method: "PUT",
          data: {status:"approved"}
      }).done(function(data) {
        if(data.status == 'error')
        {
          var errors = '<ul>';
          $.each( data.message, function( key, value )
          {
            errors = errors +'<li>'+value.msg+'</li>';
          });
          errors = errors+ '</ul>';
        }
        else
        {
          // window.location='/viewLodge';
          //document.getElementById(idDiv).classList.add('alert-danger');
          $(idDiv).html("Host has been approved!");
        }

      });

}
function rejectLodging()
{

      event.preventDefault();
      console.log(id);
      var url = "/lodging/update/"+id;
      console.log(url);
      $.ajax({
          url: url,
          method: "PUT",
          data: {status:"rejected"}
      }).done(function(data) {
        if(data.status == 'error')
        {
          var errors = '<ul>';
          $.each( data.message, function( key, value )
          {
            errors = errors +'<li>'+value.msg+'</li>';
          });
          errors = errors+ '</ul>';
        }
        else
        {
          $(idDiv).html("Host has been rejected!");
        }

      });

}
function loadBookings(){
  var image = document.getElementById('image');

  $.ajax({
      url: "./booking/allBookings",
      method: "GET"

  }).done(function(data) {
  console.log(data);
  if(data.length == 0)
  {
    $(document).find('.announcement').html("No booking requests.");
    // image.style.display = "block";
  }
  else {
    var tbl = '';
    tbl +='<table class="table listings" >'

      //--->create table header > start
      tbl +='<thead class = "listings">';
        tbl +='<tr>';
        tbl +='<th class = "listings">Lodging ID</th>';
        tbl +='<th class = "listings"> User</th>';

          tbl +='<th class = "listings">Check In Date</th>';
        tbl +='<th class = "listings">Check Out Date</th>';
        tbl +='<th class = "listings">Address</th>';
        tbl +='<th class = "listings">Guests</th>';
        tbl +='<th class = "listings">Host Status</th>';
        tbl +='<th class = "listings">Guest Status</th>';
        tbl +='<th class = "listings">Status</th>';
        tbl +='<th class = "listings">Options</th>';
        tbl +='</tr>';
      tbl +='</thead>';
      //--->create table header > end


      //--->create table body > start
      tbl +='<tbody>';
      //--->create table body rows > start
  $.each(data, function(index, val)
  {
   //you can replace with your database row id
   var row_id = val['_id'];

   var check_in = new Date(val['check_in_date']);
   var check_in_day = check_in.getDate();
   var readable_check_in = check_in.toDateString();

   var check_out = new Date(val['check_out_date']);
   var check_out_day = check_out.getDate();
   var readable_check_out = check_out.toDateString();

   //Get 1 day in milliseconds
  var one_day=1000*60*60*24;

  // Convert both dates to milliseconds
  var date1_ms =check_in.getTime();
  var date2_ms = check_out.getTime();

  // Calculate the difference in milliseconds
  var difference_ms = date2_ms - date1_ms;

  // Convert back to days and return
  var days = Math.round(difference_ms/one_day);
  var totalPrice = days*val['price']
   //loop through ajax row data
   tbl +='<tr row_id="'+row_id+'">';
   tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="lname">'+row_id+'</div></td>';
   tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="lname">'+val['user']+'</div></td>';

     tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="lname">'+readable_check_in+'</div></td>';
     tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="lname">'+readable_check_out+'</div></td>';
     tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="lname">'+val['address']+'</div></td>';
     tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="lname">'+val['guests']+'</div></td>';

     if(val['status']=='approved')
     {
       tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="status" style = "color:green;">'+val['status']+'</div></td>';
     }
     else {
       tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="status" style = "color:red;">'+val['status']+'</div></td>';
     }
     if(val['user_status']=='approved')
     {
       tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="status" style = "color:green;">'+val['user_status']+'</div></td>';
     }
     else {
       tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="status" style = "color:red;">'+val['user_status']+'</div></td>';
     }
     if(val['admin_status']=='approved')
     {
       tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="status" style = "color:green;">'+val['admin_status']+'</div></td>';
     }
     else {
       tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="status" style = "color:red;">'+val['admin_status']+'</div></td>';
     }


     //--->edit options > start
     tbl +='<td class="listings">';

       tbl +='<span class="btn_edit" > <a href="#" class="btn-link " row_id="'+row_id+'" > Edit</a> </span>';

       //only show this button if edit button is clicked
       tbl +='<span class="btn_save"> <a href="#" class="btn-link"  row_id="'+row_id+'"> Save</a> </span>';
       tbl +='<span class="btn_cancel"> <a href="#" class="btn-link" row_id="'+row_id+'"> Cancel</a></span>';

     tbl +='</td>';
     //--->edit options > end

   tbl +='</tr>';
  });

  //--->create table body rows > end

  tbl +='</tbody>';
  //--->create table body > end

  tbl +='</table>'
  //--->create data table > end

  $(document).find('.tbl_user_data').html(tbl);
  $(document).find('.tbl_user_data').html(tbl);

  $(document).find('.btn_save').hide();
	$(document).find('.btn_cancel').hide();


  $(document).on('click', '.btn_edit', function(event)
  	{
  		event.preventDefault();
  		var tbl_row = $(this).closest('tr');

  		var row_id = tbl_row.attr('row_id');

  		tbl_row.find('.btn_save').show();
  		tbl_row.find('.btn_cancel').show();

  		//hide edit button
  		tbl_row.find('.btn_edit').hide();

  		//make the whole row editable
  		tbl_row.find('.row_data')
  		.attr('contenteditable', 'true')
  		.attr('edit_type', 'button')
  		.addClass('bg-warning')
  		.css('padding','3px')

  		//--->add the original entry > start
  		tbl_row.find('.row_data').each(function(index, val)
  		{
  			//this will help in case user decided to click on cancel button
  			$(this).attr('original_entry', $(this).html());
  		});
  		//--->add the original entry > end

  	});
  	//--->button > edit > end


  	//--->button > cancel > start
  	$(document).on('click', '.btn_cancel', function(event)
  	{
  		event.preventDefault();

  		var tbl_row = $(this).closest('tr');

  		var row_id = tbl_row.attr('row_id');

  		//hide save and cacel buttons
  		tbl_row.find('.btn_save').hide();
  		tbl_row.find('.btn_cancel').hide();

  		//show edit button
  		tbl_row.find('.btn_edit').show();

  		//make the whole row editable
  		tbl_row.find('.row_data')
  		.attr('edit_type', 'click')
  		.removeClass('bg-warning')
  		.css('padding','')

  		tbl_row.find('.row_data').each(function(index, val)
  		{
  			$(this).html( $(this).attr('original_entry') );
  		});
  	});
  //--->save whole row entery > start
  	$(document).on('click', '.btn_save', function(event)
  	{
  		event.preventDefault();
  		var tbl_row = $(this).closest('tr');

  		var row_id = tbl_row.attr('row_id');


  		//hide save and cacel buttons
  		tbl_row.find('.btn_save').hide();
  		tbl_row.find('.btn_cancel').hide();

  		//show edit button
  		tbl_row.find('.btn_edit').show();


  		//make the whole row editable
      tbl_row.find('.row_data')
  		.attr('edit_type', 'click')
  		.removeClass('bg-warning')
  		.css('padding','')

  		tbl_row.find('.row_data').each(function(index, val)
  		{
        if(index!=8)
        {
          $(this).html( $(this).attr('original_entry') );
        }
        else {
          var col_val  =  $(this).html();
          if(col_val == "approved")
          {
            $.ajax({
              url:"booking/updateAdmin/"+row_id,
              method:"PUT",
              data:{status:col_val}
            }).done(function(data){
              console.log("success");
            });
          }
          else if (col_val == "rejected") {
            $.ajax({
              url:"booking/updateAdmin/"+row_id,
              method:"PUT",
              data:{status:col_val}
            }).done(function(data){
              console.log("success");
            });
          }
          else if (col_val == "pending") {
            $.ajax({
              url:"booking/updateAdmin/"+row_id,
              method:"PUT",
              data:{status:col_val}
            }).done(function(data){
              console.log("success");
            });
          }
          else {
            $(this).html( $(this).attr('original_entry') );
          }
        }

  		});


  	});
  	//--->save whole row entery > end
  }

});
}
$(function(){

    $("#logout-button").on('click', function(event){
        event.preventDefault();
            $.ajax({
                url: "/logout",
                method: "GET",

            }).done(function(data) {

                window.location = ('/adminLogin');
            });
        });
    });
    function move() {
      var elem = document.getElementById("myBar");
      var width = 1;
      var id = setInterval(frame, 10);
      function frame() {
        if (width >= 100) {
          clearInterval(id);
        } else {
          width++;
          elem.style.width = width + '%';
        }
      }
    }
    $(function(){

        $("#adminData-button").on('click', function(event){
            event.preventDefault();
                $.ajax({
                    url: "/dataAnalysis",
                    method: "GET",

                }).done(function(data) {

                    window.location = ('/dataAnalysis');
                });
            });
        });
window.onload = loadBookings();
