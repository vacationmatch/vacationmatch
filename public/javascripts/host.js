var lodging = "";
function loadListings(){
  var image = document.getElementById('image');

  $.ajax({
      url: "./myListing",
      method: "GET"

  }).done(function(data) {
  console.log(data);
  if(data.length == 0)
  {
    $(document).find('.announcement').html("No listings. Fill out the host form to publish your lodging!");
    image.style.display = "block";
  }
  else {
    var title = document.getElementById('title');
    title.style.display = "block";
    var tbl = '';
    tbl +='<table class="table listings" >'

      //--->create table header > start
      tbl +='<thead class = "listings">';
        tbl +='<tr>';
        tbl +='<th class = "listings">Lodging ID</th>';
        tbl +='<th class = "listings">Type</th>';
        tbl +='<th class = "listings">Address</th>';
        tbl +='<th class = "listings">Maximum Guests</th>';
        tbl +='<th class = "listings">Rooms</th>';
        tbl +='<th class = "listings">Bathrooms</th>';
        tbl +='<th class = "listings">Price</th>';
        tbl +='<th class = "listings">Status</th>';
        tbl +='<th class = "listings">Options</th>';
        tbl +='</tr>';
      tbl +='</thead>';
      //--->create table header > end


      //--->create table body > start
      tbl +='<tbody>';
      //--->create table body rows > start
  $.each(data, function(index, val)
  {
   //you can replace with your database row id
   var row_id = val['_id'];

   //loop through ajax row data
   tbl +='<tr row_id="'+row_id+'">';
   tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="type">'+val['_id']+'</div></td>';

     tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="type">'+val['type']+'</div></td>';
     tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="address">'+val['address']+'</div></td>';
     tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="maxCap">'+val['maxCap']+'</div></td>';
     tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="numRooms">'+val['numRooms']+'</div></td>';
     tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="numBathrooms">'+val['numBathrooms']+'</div></td>';

     tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="price">'+val['price']+'</div></td>';
     if(val['status']=='approved')
     {
       tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="status" style = "color:green;">'+val['status']+'</div></td>';
     }
     else {
       tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="status" style = "color:red;">'+val['status']+'</div></td>';
     }


     //--->edit options > start
     tbl +='<td class = "listings">';

       tbl +='<span class="btn_edit" > <a href="#" class="btn-link " row_id="'+row_id+'" > Edit</a> </span>';

       //only show this button if edit button is clicked
       tbl +='<span class="btn_save"> <a href="#" class="btn-link"  row_id="'+row_id+'"> Save</a> | </span>';
       tbl +='<span class="btn_cancel"> <a href="#" class="btn-link" row_id="'+row_id+'"> Cancel</a> | </span>';

     tbl +='</td>';
     //--->edit options > end

   tbl +='</tr>';
  });

  //--->create table body rows > end

  tbl +='</tbody>';
  //--->create table body > end

  tbl +='</table>'
  //--->create data table > end

  $(document).find('.tbl_user_data').html(tbl);

  $(document).find('.btn_save').hide();
	$(document).find('.btn_cancel').hide();


  $(document).on('click', '.btn_edit', function(event)
  	{
  		event.preventDefault();
  		var tbl_row = $(this).closest('tr');

  		var row_id = tbl_row.attr('row_id');

  		tbl_row.find('.btn_save').show();
  		tbl_row.find('.btn_cancel').show();

  		//hide edit button
  		tbl_row.find('.btn_edit').hide();

  		//make the whole row editable
  		tbl_row.find('.row_data')
  		.attr('contenteditable', 'true')
  		.attr('edit_type', 'button')
  		.addClass('bg-warning')
  		.css('padding','3px')

  		//--->add the original entry > start
  		tbl_row.find('.row_data').each(function(index, val)
  		{
  			//this will help in case user decided to click on cancel button
  			$(this).attr('original_entry', $(this).html());
  		});
  		//--->add the original entry > end

  	});
  	//--->button > edit > end


  	//--->button > cancel > start
  	$(document).on('click', '.btn_cancel', function(event)
  	{
  		event.preventDefault();

  		var tbl_row = $(this).closest('tr');

  		var row_id = tbl_row.attr('row_id');

  		//hide save and cacel buttons
  		tbl_row.find('.btn_save').hide();
  		tbl_row.find('.btn_cancel').hide();

  		//show edit button
  		tbl_row.find('.btn_edit').show();

  		//make the whole row editable
  		tbl_row.find('.row_data')
  		.attr('edit_type', 'click')
  		.removeClass('bg-warning')
  		.css('padding','')

  		tbl_row.find('.row_data').each(function(index, val)
  		{
  			$(this).html( $(this).attr('original_entry') );
  		});
  	});
  //--->save whole row entery > start
  	$(document).on('click', '.btn_save', function(event)
  	{
  		event.preventDefault();
  		var tbl_row = $(this).closest('tr');

  		var row_id = tbl_row.attr('row_id');


  		//hide save and cacel buttons
  		tbl_row.find('.btn_save').hide();
  		tbl_row.find('.btn_cancel').hide();

  		//show edit button
  		tbl_row.find('.btn_edit').show();


  		//make the whole row editable
  		tbl_row.find('.row_data')
  		.attr('edit_type', 'click')
  		.removeClass('bg-warning')
  		.css('padding','')

  		//--->get row data > start
  		var arr = {};
  		tbl_row.find('.row_data').each(function(index, val)
  		{
  			var col_name = $(this).attr('col_name');
  			var col_val  =  $(this).html();
  			arr[col_name] = col_val;
  		});
  		//--->get row data > end
      console.log(arr);
  		//use the "arr"	object for your ajax call
  		$.extend(arr, {row_id:row_id});

  		//out put to show
  		$('.post_msg').html( '<pre class="bg-success">'+JSON.stringify(arr, null, 2) +'</pre>')


  	});
  	//--->save whole row entery > end


  }

});
}
function loadRequests(){
  var image = document.getElementById('image');

  $.ajax({
      url: "../booking/lodging",
      method: "GET"

  }).done(function(data) {
  console.log(data);
  if(data.length == 0)
  {

    image.style.display = "none";
  }
  else {
    var requests = document.getElementById('requests');
    requests.style.display = "block";
    var tbl = '';
    tbl +='<table class="table listings" >'

      //--->create table header > start
      tbl +='<thead class = "listings">';
        tbl +='<tr>';

        tbl +='<th class = "listings">Lodging ID</th>';
        tbl +='<th class = "listings">User</th>';
        tbl +='<th class = "listings">Guests</th>';
        tbl +='<th class = "listings">Check In Date</th>';
        tbl +='<th class = "listings">Check Out Date</th>';
        tbl +='<th class = "listings">Guest Status</th>';
        tbl +='<th class = "listings">Status</th>';
        tbl +='<th class = "listings">Options</th>';
        tbl +='</tr>';
      tbl +='</thead>';
      //--->create table header > end


      //--->create table body > start
      tbl +='<tbody>';
      //--->create table body rows > start
      console.log(data);
  $.each(data, function(index, val)
  {
   //you can replace with your database row id
   var row_id = val['_id'];
   var check_in = new Date(val['check_in_date']);
   var readable_check_in = check_in.toDateString();
   var check_out = new Date(val['check_out_date']);
   var readable_check_out = check_out.toDateString();



   //loop through ajax row data
   tbl +='<tr row_id="'+row_id+'">';
     tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="address">'+val['lodgingId']+'</div></td>';
     tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="maxCap">'+val['user']+'</div></td>';
     tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="maxCap">'+val['guests']+'</div></td>';

     tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="price">'+readable_check_in+'</div></td>';
     tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="price">'+readable_check_out+'</div></td>';

     if(val['user_status']=='approved')
     {
       tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="status" style = "color:green;">'+val['user_status']+'</div></td>';
     }
     else {
       tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="status" style = "color:red;">'+val['user_status']+'</div></td>';
     }
     if(val['status']=='approved')
     {
       tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="status" style = "color:green;">'+val['status']+'</div></td>';
     }
     else {
       tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="status" style = "color:red;">'+val['status']+'</div></td>';
     }


     //--->edit options > start
     tbl +='<td class="listings">';

       tbl +='<span class="btn_edit" > <a href="#" class="btn-link " row_id="'+row_id+'" > Edit</a> </span>';

       //only show this button if edit button is clicked
       tbl +='<span class="btn_save"> <a href="#" class="btn-link"  row_id="'+row_id+'"> Save</a> | </span>';
       tbl +='<span class="btn_cancel"> <a href="#" class="btn-link" row_id="'+row_id+'"> Cancel</a> | </span>';

     tbl +='</td>';
     //--->edit options > end

   tbl +='</tr>';
  });

  //--->create table body rows > end

  tbl +='</tbody>';
  //--->create table body > end

  tbl +='</table>'
  //--->create data table > end

  $(document).find('.tbl_request_data').html(tbl);

  $(document).find('.btn_save').hide();
	$(document).find('.btn_cancel').hide();


  $(document).on('click', '.btn_edit', function(event)
  	{
  		event.preventDefault();
  		var tbl_row = $(this).closest('tr');

  		var row_id = tbl_row.attr('row_id');

  		tbl_row.find('.btn_save').show();
  		tbl_row.find('.btn_cancel').show();

  		//hide edit button
  		tbl_row.find('.btn_edit').hide();

  		//make the whole row editable
  		tbl_row.find('.row_data')
  		.attr('contenteditable', 'true')
  		.attr('edit_type', 'button')
  		.addClass('bg-warning')
  		.css('padding','3px')

  		//--->add the original entry > start
  		tbl_row.find('.row_data').each(function(index, val)
  		{
  			//this will help in case user decided to click on cancel button
  			$(this).attr('original_entry', $(this).html());
  		});
  		//--->add the original entry > end

  	});
  	//--->button > edit > end


  	//--->button > cancel > start
  	$(document).on('click', '.btn_cancel', function(event)
  	{
  		event.preventDefault();

  		var tbl_row = $(this).closest('tr');

  		var row_id = tbl_row.attr('row_id');

  		//hide save and cacel buttons
  		tbl_row.find('.btn_save').hide();
  		tbl_row.find('.btn_cancel').hide();

  		//show edit button
  		tbl_row.find('.btn_edit').show();

  		//make the whole row editable
  		tbl_row.find('.row_data')
  		.attr('edit_type', 'click')
  		.removeClass('bg-warning')
  		.css('padding','')

  		tbl_row.find('.row_data').each(function(index, val)
  		{
  			$(this).html( $(this).attr('original_entry') );
  		});
  	});
  //--->save whole row entery > start
  	$(document).on('click', '.btn_save', function(event)
  	{
  		event.preventDefault();
  		var tbl_row = $(this).closest('tr');

  		var row_id = tbl_row.attr('row_id');


  		//hide save and cacel buttons
  		tbl_row.find('.btn_save').hide();
  		tbl_row.find('.btn_cancel').hide();

  		//show edit button
  		tbl_row.find('.btn_edit').show();


  		//make the whole row editable
  		tbl_row.find('.row_data')
  		.attr('edit_type', 'click')
  		.removeClass('bg-warning')
  		.css('padding','')
      //--->get row data > start
      tbl_row.find('.row_data').each(function(index, val)
      {
        if(index!=6)
        {
          $(this).html( $(this).attr('original_entry') );
        }
        else {
          var col_val  =  $(this).html();
          if(col_val == "approved")
          {
            $.ajax({
              url:"/booking/updateHost/"+row_id,
              method:"PUT",
              data:{status:col_val}
            }).done(function(data){
              console.log("success");
            });
          }
          else if (col_val == "rejected") {
            $.ajax({
              url:"/booking/updateHost/"+row_id,
              method:"PUT",
              data:{status:col_val}
            }).done(function(data){
              console.log("success");
            });
          }
          else if (col_val == "pending") {
            $.ajax({
              url:"/booking/updateHost/"+row_id,
              method:"PUT",
              data:{status:col_val}
            }).done(function(data){
              console.log("success");
            });
          }
          else {
            $(this).html( $(this).attr('original_entry') );
          }
        }

      });

      });
      //--->save whole row entery > end


  }

});
}

	//--->button > cancel > end
$(function(){

    $("#step-1").on('click', function(event){
        event.preventDefault();

        var type   = $("#type").val();
        var address     = $("#address").val();
        var city   = $("#city").val();
        var state  = "Texas";
        var zipcode   = $("#zipcode").val();
        var maxCap     = $("#maxCap").val();
        var numRooms     = $("#rooms").val();
        var numBathrooms    = $("#bathrooms").val();
      //  var attachments = document.getElementById("attachments").files[0].name;
        //document.getElementById("fileToUpload").files[0].name;
        if(!type || !address || !city || !state || !zipcode || !maxCap  || !numRooms || !numBathrooms){

            $("#msgDiv").show().html("All fields are required.");
        }
        else{
          var status = "pending";
              $.ajax({
                  url: "lodging/insertSurvey",
                  method: "POST",
                  data: {type:type, address: address, city:city, state:"Texas",zipcode:zipcode, maxCap: maxCap, numRooms: numRooms,numBathrooms:numBathrooms,status:status },


              }).done(function( data ) {

                  if ( data ) {
                      if(data.status == 'error')
                      {
                          var errors = '';
                          $.each( data.message, function( key, value )
                          {
                            errors = errors +value.msg+'<br>';
                          });

                          $("#msgDiv").html(errors).show();
                      }
                      else
                      {
                        console.log(data);
                        lodging = data.id;
                        var form1 = document.getElementById("form-host");
                        var pictureDiv = document.getElementById("pictures");
                        form1.style.display="none";
                        pictureDiv.style.display="block";
                      }
                    }
                  });
                }
              });
});
$(function(){

    $("#host").on('click', function(event){
        event.preventDefault();
        var description   = $("#description-text").val();
        var price = $("#price").val();
        var url = "lodging/updateDescription/"+lodging;
            $.ajax({
                url: url,
                method: "PUT",
                data: {description:description, price:price},


            }).done(function( data ) {

                if ( data ) {
                    if(data.status == 'error')
                    {
                        var errors = '<ul>';
                        $.each( data.message, function( key, value )
                        {
                          errors = errors +'<li>'+value.msg+'</li>';
                        });
                        errors = errors+ '</ul>';
                        console.log(errors);
                    }
                    else
                    {
                      lodging = data.id;
                      window.location = "/thanks";
                    }
                  }
                });
              });
});

//upload pictures from front end to database
$(function(){

    $("#sumbitPictures").on('click', function(event){
        event.preventDefault();
        var form = $('#uploadPictures')[0];
        var data = new FormData(form);
        data.append("id",lodging);
        console.log(data);
            $.ajax({
                url: 'lodging/uploadPictures',
                method: "POST",
                enctype: 'multipart/form-data',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
            }).done(function( data ) {
                    if(data.status == 'error')
                    {
                        var errors = '<ul>';
                        $.each( data.message, function( key, value )
                        {
                          errors = errors +'<li>'+value.msg+'</li>';
                        });
                        errors = errors+ '</ul>';
                        console.log(errors);
                    }
                    else
                    {
                      var descriptionDiv = document.getElementById("description");
                      var pictureDiv = document.getElementById("pictures");
                      pictureDiv.style.display = "none";
                      descriptionDiv.style.display = "block";
                    }
                  });

                });
              });

function loadAll(){
  loadListings();
  loadRequests();
}
window.onload = loadAll();
