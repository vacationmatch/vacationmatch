var express   = require('express');
var router    = express.Router();
var mongoose  = require('mongoose');
var multer = require('multer');
var session = require('express-session');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
const ObjectId = require('mongodb').ObjectID;

var Booking = mongoose.model('Booking');

/* GET users listing. */
router.get('/booking/:id', function(req, res, next) {
  var id = new mongoose.Types.ObjectId(req.params.lodging);
  Booking.findOne({_id:id}).exec(function(err, lodging)
  {
    if(err){
      console.log('No such booking exists');
      return next(err)
    }
    else
    {
      res.json(lodging);
    }
  });
});
router.get('/lodging', function(req, res, next) {
  var user = req.session.user;
  var email = user.email;
  Booking.find({host:email}).exec(function(err, booking)
  {
    if(err){
      console.log('No such booking exists');
      return next(err)
    }
    else
    {
      console.log(booking);
      res.json(booking);
    }
  });
});
router.get('/allBookings', function(req, res, next) {
  Booking.find({}).exec(function(err, lodging)
  {
    if(err){
      console.log('No such booking exists');
      return next(err)
    }
    else
    {
      res.json(lodging);
    }
  });
});
router.post('/insertBooking', function(req, res, next)
  {
    const lodgingId = req.body.lodgingId;
    const host = req.body.host;
    const guests = req.body.guests;
    const check_in_date = req.body.check_in_date;
    const check_out_date = req.body.check_out_date;
    const check_in_time = req.body.check_in_time;
    const check_out_time = req.body.check_out_time;
    const status = req.body.status;
    const user_status = "pending";
    const admin_status = "pending";
    // const pending = 'pending';
    // const bookee = req.session.user;
    // const email = bookee.email;

    const document = {
      lodgingId:lodgingId,
      host: host,
      guests:guests,
      check_in_date: check_in_date,
      check_out_date: check_out_date,
      check_in_time: check_in_time,
      check_out_time: check_out_date,
      status: status
    };
    console.log(document);

    var booking = new Booking(document);
    console.log(booking)
    booking.save(function(error){
      if(error){
        console.log('No such booking exists');
        throw error;
      }
      else {
        console.log(booking);
        res.json({lodgingId: lodgingId});
      }
    });

  });
  router.get('/myTrips', function(req,res,next){
    user = req.session.user;
    email = user.email;
    Booking.find({user:email}).exec(function(err, booking)
    {
      if(err){
        console.log('There are no requests');
        return next(err)
      }
      else
    {
      console.log(booking);
      res.json(booking);
    }
    });

  });
  router.post('/insertBooking2', function (req,res) {
    const user = req.session.user;
    const email = user.email;
    const lodgingId = req.body.lodgingId;
    const address = req.body.address;
    const host = req.body.host;
    const guests = req.body.guests;
    const check_in_date = req.body.check_in_date;
    const check_out_date = req.body.check_out_date;
    const price = req.body.price;
    const user_status = "pending";
    const admin_status = "pending";
    // const check_in_time = req.body.check_in_time;
    // const check_out_time = req.body.check_out_time;
    //const status = req.body.status;
    const check_in_time = "10:00AM";
    const check_out_time = "10:00AM"
    const status = 'pending';
    console.log(user);
    console.log(email);
    console.log(lodgingId);
    console.log(host);
    console.log(guests);
    console.log(check_in_date);
    console.log(check_out_date);
    const data = {
      'lodgingId':lodgingId,
      'address':address,
      'host': host,
      'guests':guests,
      'check_in_date': check_in_date,
      'check_out_date': check_out_date,
      'check_in_time': check_in_time,
      'check_out_time': check_out_date,
      'user':email,
      'price':price,
      'status': status,
      'user_status':user_status,
      'admin_status':admin_status
    };
    console.log(data);

    var booking = new Booking(data);
    booking.save()
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });

    console.log("here")
    res.status(201).json({"booking": booking});
  });

//booking or lodging?
  router.put('/updateAdmin/:booking',function(req,res)
    {
      var status = req.body.status;
      var id = new mongoose.Types.ObjectId(req.params.booking);
      var newStatus = { $set: {admin_status:status} };
      Booking.findByIdAndUpdate(id, newStatus,
        function(err, booking)
        {
          if (err) return next(err);
          res.send("booking status updated");
        });
    });

    router.put('/update/:booking',function(req,res)
      {
        var status = req.body.status;
        var id = new mongoose.Types.ObjectId(req.params.booking);
        var newStatus = { $set: {user_status:status} };
        Booking.findByIdAndUpdate(id, newStatus,
          function(err, booking)
          {
            if (err) return next(err);
            res.send("booking status updated");
          });
      });
      router.put('/updateHost/:booking',function(req,res)
        {
          var status = req.body.status;
          var id = new mongoose.Types.ObjectId(req.params.booking);
          var newStatus = { $set: {status:status} };
          Booking.findByIdAndUpdate(id, newStatus,
            function(err, booking)
            {
              if (err) return next(err);
              res.send("booking status updated");
            });
        });



module.exports = router;
