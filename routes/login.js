var express   = require('express');
var router    = express.Router();
var mongoose  = require('mongoose');
var User      = mongoose.model('User');
var crypto    = require('crypto'), hmac, signature;
const { check, validationResult } = require('express-validator/check');
const { matchedData, sanitize }   = require('express-validator/filter');


router.post('/login', function(req, res, next) {
  if (req.body.emailLogin && req.body.passwordLogin)
  {
    User.authenticate(req.body.emailLogin, req.body.passwordLogin, function (error, user)
    {
      if (error || !user)
      {
        var err = new Error('Wrong email or password.');
        err.status = 401;
        return next(err);
      }
      else
      {
        //needed when you implement session
        //req.session.userId = user._id;
        return res.redirect('/home');
      }
    });
  }
  else
  {
    var err = new Error('All fields required.');
    err.status = 400;
    return next(err);
  }
});

module.exports = router;
